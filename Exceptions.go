package Exceptions
import (
	"fmt"
	"path"
	"runtime"
)

type Exception struct {
	Payload  interface{}
	Domain   string
	Code     int

	fileName string
	line     int
}

func (e Exception) Error() string {
	return fmt.Sprintf("%s:%d: %+v [%s:%d]", e.fileName, e.line, e.Payload, e.Domain, e.Code)
}

// EXCEPTIONS MECHANISM

func Throw(i interface{}) {
	if e, ok := i.(Exception); ok {
		_throw(e)
	}else {
		_throw(Exception{Payload:i})
	}
}

func _throw(e Exception) {
	_, file, line, _ := runtime.Caller(2)
	_, file = path.Split(file)
	e.fileName = file
	e.line = line
	panic(e)
}


func Catch(handler func(e Exception)) {
	if r := recover(); r != nil {
		if e, ok := r.(Exception); ok {
			handler(e)
		} else {
			handler(Exception{Payload:r})
		}
	}
}

func BS(b bool, s string) {
	if b {
		_throw(Exception{Payload:s})
	}
}

func E(err error) {
	if err != nil {
		_throw(Exception{Payload:err.Error()})
	}
}

func ES(err error, s string) {
	if err != nil {
		_throw(Exception{Payload: fmt.Sprintf("%s (%s)", s, err.Error())})
	}
}